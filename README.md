# wordpressexport2html

A hacky little script that reads a WordPress export XML file and creates some
HTML files from it.

I use it to copy my <a href="https://artificialworlds.net/blog">blog</a> to
<a href="https://gemini.circumlunar.space/">Gemini</a>.

(The copy of my blog is at
`gemini://republic.circumlunar.space/users/andybalaam/blogmirror/`.)

If you want to use it, you will probably need to change it to fit your needs:
I welcome contributions.

## Usage

* Export your blog in WordPress by choosing Dashboard -> Tools -> Export
* Choose to export Posts only
* Run wordpressexport2html:
    ```bash
    ./wordpressexport2html < path/to/export.xml
    ```
* This will create a directory called `htmlfiles` and create one HTML file
  inside it for each post in the blog.

## Converting a WordPress blog to Gemini

I wrote this to allow me to copy my blog into Gemini.  The full procedure I
follow is:

* Export posts in the WordPress web UI, and save the export file as `exp.xml`
* Convert to HTML:
    ```bash
    rm -r htmlfiles
    ./wordpressexport2html < exp.xml
    ```
* Convert to Gemini with
  <a href="https://github.com/LukeEmmet/html2gmi">html2gmi</a>:
    ```bash
    cd htmlfiles
    mkdir ../gmifiles
    for F in *.html; do
        html2gmi < "$F" > "${F%html}gmi"
    done
    ```

## Contributing

Please do!  Anything that continues to work for me and also makes it more
useful for others is likely to be accepted.

Please run `autopep8 -i wordpressexport2html` before submitting a merge
request.

## License and copyright

wordpressexport2html is Copyright 2020 Andy Balaam and is released under the
[Apache 2](LICENSE) license.
